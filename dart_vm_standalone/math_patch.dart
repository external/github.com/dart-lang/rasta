// Copyright (c) 2016, the Dart project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE.md file.

library dart.math_patch;

import 'dart:_patch' show
    patch;

import 'dart:typed_data' show
    Uint32List;

part '../third_party/dart-sdk/runtime/lib/math_patch.dart';
