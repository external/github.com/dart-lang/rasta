// Copyright (c) 2016, the Dart project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE.md file.

library dart.io_patch;

import 'dart:_patch' show
    patch;

import 'dart:nativewrappers' show
    NativeFieldWrapperClass1;

part '../third_party/dart-sdk/runtime/bin/common_patch.dart';
part '../third_party/dart-sdk/runtime/bin/directory_patch.dart';
part '../third_party/dart-sdk/runtime/bin/eventhandler_patch.dart';
part '../third_party/dart-sdk/runtime/bin/file_patch.dart';
part '../third_party/dart-sdk/runtime/bin/file_system_entity_patch.dart';
part '../third_party/dart-sdk/runtime/bin/filter_patch.dart';
part '../third_party/dart-sdk/runtime/bin/io_service_patch.dart';
part '../third_party/dart-sdk/runtime/bin/platform_patch.dart';
part '../third_party/dart-sdk/runtime/bin/process_patch.dart';
// TODO(ahe): I suspect this is broken:
// part '../third_party/dart-sdk/runtime/bin/service_object_patch.dart';
part '../third_party/dart-sdk/runtime/bin/socket_patch.dart';
part '../third_party/dart-sdk/runtime/bin/stdio_patch.dart';
part '../third_party/dart-sdk/runtime/bin/secure_socket_patch.dart';
