// Copyright (c) 2016, the Dart project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE.md file.

library rasta.unimplemented;

// TODO(ahe): Eventually we should get rid of all references to this and delete
// it.
get unimplemented => throw "not implemented";
