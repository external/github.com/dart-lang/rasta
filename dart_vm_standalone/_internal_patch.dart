// Copyright (c) 2016, the Dart project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE.md file.

library dart._internal_patch;

import 'dart:_patch' show
    patch;

part '../third_party/dart-sdk/runtime/lib/internal_patch.dart';
part '../third_party/dart-sdk/runtime/lib/print_patch.dart';
part '../third_party/dart-sdk/runtime/lib/symbol_patch.dart';
part '_internal/class_id.dart';
