// Copyright (c) 2016, the Dart project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE.md file.

library dart.collection_patch;

import 'dart:_patch' show
    patch;

import 'dart:typed_data' show
    Uint32List;

import 'dart:_internal' as internal;

part '../third_party/dart-sdk/runtime/lib/collection_patch.dart';
part '../third_party/dart-sdk/runtime/lib/compact_hash.dart';
