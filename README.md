<!--
Copyright (c) 2016, the Dart project authors.  Please see the AUTHORS file
for details. All rights reserved. Use of this source code is governed by a
BSD-style license that can be found in the LICENSE file.
-->
# Resolved Abstract Syntax Tree Apparatus (Rasta)

## Getting started

After cloning this repository, please run `git submodule update --init` to
fetch dependencies.

## Committing changes

When committing a change, please use [commit.sh]. If that script doesn't match
your workflow, make sure to run tests before committing.

## Running tests

This command runs all the tests:

```
dart -c test/testa.dart
```
